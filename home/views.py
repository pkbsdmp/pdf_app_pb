from django.shortcuts import render, HttpResponse, redirect, reverse
from django.shortcuts import render as render_template
import os
import base64
from datetime import datetime
import json
from collections import OrderedDict
from werkzeug.utils import secure_filename
from .utils import *
from pathlib import Path
from django.http import Http404
# Create your views here.
from django.contrib import messages
from django.contrib.auth.decorators import login_required


@login_required
def index(request):
    if request.method == "POST":
        pass
    return render_template(request, 'home.html', provider_data())


@login_required
def create_pdf_data(request):

    if request.method == "POST":
        send_email = request.POST.get('send_email', '')
        document_name = request.POST.get('document_name', '').strip()
        client_name = request.POST.get('client_name', '')
        client_address = request.POST.get('client_address', '')
        client_phone = request.POST.get('client_phone', '')
        client_email = request.POST.get('client_email', '')
        terms_of_service = request.POST.get('terms_of_service', '')
        estimated_cost = request.POST.get('estimated_cost', '')
        deliverable = request.POST.get('deliverable', '')

        total_page = 1
        page_two = request.FILES.get('page_two')
        page_two_content = str()
        if page_two:
            save_inmem_file(page_two, os.path.join(TEMP_DIR, 'page_two.txt'))
            with open(os.path.join(TEMP_DIR, 'page_two.txt'), 'r', encoding='utf-8', errors="ignore") as f:
                page_two_content = f.read()
            os.remove(os.path.join(TEMP_DIR, 'page_two.txt'))
            total_page += 1

        page_three = request.FILES.get('page_three')
        page_three_content = str()
        if page_three:
            save_inmem_file(page_three, os.path.join(TEMP_DIR, 'page_three.txt'))
            with open(os.path.join(TEMP_DIR, 'page_three.txt'), 'r', encoding='utf-8', errors="ignore") as f:
                page_three_content = f.read()
            os.remove(os.path.join(TEMP_DIR, 'page_three.txt'))
            total_page += 1

        page_four = request.FILES.get('page_four')
        page_four_content = str()
        if page_four:
            save_inmem_file(page_four, os.path.join(TEMP_DIR, 'page_four.txt'))
            with open(os.path.join(TEMP_DIR, 'page_four.txt'), 'r', encoding='utf-8', errors="ignore") as f:
                page_four_content = f.read()
            os.remove(os.path.join(TEMP_DIR, 'page_four.txt'))
            total_page += 1

        payment_url = request.POST.get('payment_url', '')

        provider = provider_data()
        b_name, poc = "", ""
        try:
            b_name, poc = client_name.split("/")
        except:
            pass

        dict_to_send = dict(payment_url=payment_url,
                            document_name=document_name,
                            client_name=client_name,
                            client_address=client_address,
                            client_phone=client_phone,
                            client_email=client_email,
                            terms_of_service=terms_of_service,
                            estimated_cost=estimated_cost,
                            deliverable=deliverable,
                            page_two_content=page_two_content,
                            page_three_content=page_three_content,
                            page_four_content=page_four_content,
                            total_page=total_page,
                            b_name=b_name, poc=poc)

        dict_to_send.update(provider)
        if send_email and send_email == "True":
            ret, msg = create_pdf_and_send_email(dict_to_send, document_name)
            provider_details = provider_data()
            if ret:
                provider_details.update(dict(sent_mail=True))
                return render_template(request, 'home.html', provider_details)
            else:
                provider_details.update(dict(not_sent_mail=True, msg=msg))
                return render_template(request, 'home.html', provider_details)
        else:
            return create_pdf_util(dict_to_send, document_name)

    else:
        return render_template(request, 'home.html', provider_data())


@login_required
def load_pdf(request):
    if request.method == "POST":
        document_name = request.FILES.get('pdf_file')
        with open(os.path.join(TEMP_DIR, secure_filename(document_name) + '.json')) as f:
            data = json.load(f)
        data_to_upload = provider_data()
        data_to_upload.update(data)
        return render_template(request, 'home.html', data_to_upload)

    else:
        file_list = [f for f in os.listdir(PDF_FILES_DIR) if f.endswith('.pdf')]
        provider_data_dict = provider_data()
        provider_data_dict.update({'file_list': file_list})
        return render_template(request, 'load_pdf.html', provider_data_dict)


@login_required
def provider_contact(request):
    if request.method == "POST":
        file_object = request.FILES.get('logo')
        contacts = OrderedDict(request.POST)
        file_name = os.path.join(TEMP_DIR, 'logo.png')
        with open(file_name, 'wb+') as f:
            for chunk in file_object.chunks():
                f.write(chunk)

        with open(PROVIDER_FILE, 'w') as f:
            json.dump(contacts, f)
        return redirect(reverse('home'))
    else:
        return render_template(request, 'provider_contact.html', provider_data())


@login_required
def email_settings(request):
    if request.method == "POST":
        contacts = OrderedDict(request.POST)
        with open(EMAIL_FILE, 'w') as f:
            json.dump(contacts, f)
        return redirect(reverse('home'))
    else:
        prov_data = provider_data()
        prov_data.update(email_data())
        return render_template(request, 'email_settings.html', prov_data)


@login_required
def select_pdf_file(request):
    file_name = request.GET.get("filename")
    document_name = os.path.splitext(file_name)[0]
    with open(os.path.join(TEMP_DIR, document_name + '.json')) as f:
        data = json.load(f)
    data_to_upload = provider_data()
    data_to_upload.update(data)
    return render_template(request, 'home.html', data_to_upload)


@login_required
def delete_pdf_file(request):
    file_name = request.GET.get("filename")
    document_name = os.path.splitext(file_name)[0]
    pdf_filepath = os.path.join(PDF_FILES_DIR, file_name)
    json_file = os.path.join(TEMP_DIR, document_name + '.json')
    os.remove(pdf_filepath)
    os.remove(json_file)
    messages.success(request, "Pdf file '{}' deleted".format(file_name))
    return redirect(reverse('load_pdf'))

    # file_path = str(Path(PDF_FILES_DIR) / file_name)
    # if not os.path.exists(file_path):
    #     raise Http404("Could not find file")
    # with open(file_path, 'rb') as f:
    #     response = HttpResponse(f.read(), content_type='application/pdf')
    # response['Content-Type'] = 'application/pdf'
    # response['Content-Disposition'] = 'inline;filename={}'.format(file_name)
    # return response
