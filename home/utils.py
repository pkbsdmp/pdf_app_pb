from django.shortcuts import render, HttpResponse
from django.shortcuts import render as render_template
from django.template import loader
import os
import base64
import json
import pdfkit
from PyPDF2 import PdfFileMerger
import platform
from werkzeug.utils import secure_filename
from datetime import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication

from django.conf import settings
BASE_DIR = settings.BASE_DIR
TEMP_DIR = os.path.join(BASE_DIR, 'temp')
css_path = os.path.join(BASE_DIR, 'static/css/bootstrap.min.css')
CSS = [css_path]

if 'win' in platform.system().lower():
    PATH_WKTHMLTOPDF = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
else:
    PATH_WKTHMLTOPDF = '/usr/bin/wkhtmltopdf'

if not os.path.exists(TEMP_DIR):
    os.makedirs(TEMP_DIR)

PDF_FILES_DIR = os.path.join(BASE_DIR, 'pdf_files')
if not os.path.exists(PDF_FILES_DIR):
    os.makedirs(PDF_FILES_DIR)

PROVIDER_FILE = os.path.join(TEMP_DIR, 'prov_details.json')
EMAIL_FILE = os.path.join(TEMP_DIR, 'email_file.json')


class SendEmail(object):
    def __init__(self, smtp_address, port, username, password):
        self.server_address = smtp_address + ':' + str(port)
        self.username = username
        self.password = password
        self.server = None
        self.login()

    def __del__(self):
        if self.server:
            self.server.quit()

    def login(self):
        self.server = smtplib.SMTP(self.server_address)
        self.server.starttls()
        self.server.login(self.username, self.password)

    def send_email(self, sender, recipient, subject, body, path, filename):

        try:
            outer = MIMEMultipart()
            outer['Subject'] = subject
            outer['To'] = recipient
            outer['From'] = sender
            msg = MIMEText(body, 'html')
            outer.attach(msg)

            fp = open(path, 'rb')

            att = MIMEApplication(fp.read(), _subtype="pdf")
            fp.close()
            att.add_header('Content-Disposition', 'attachment', filename=filename)
            outer.attach(att)
            composed = outer.as_string()
            self.server.sendmail(sender, recipient, composed)
            return 1, "Success"
        except Exception as e:
            return None, str(e)

def provider_data():
    if not (os.path.exists(PROVIDER_FILE) and os.path.exists(os.path.join(TEMP_DIR, 'logo.png'))):
        return dict()

    with open(os.path.join(TEMP_DIR, 'logo.png'), "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode("utf-8", "ignore")

    with open(PROVIDER_FILE, 'r') as f:
        provider_contacts_data = json.load(f)

    business_title = provider_contacts_data.get('business_title', '')
    list_data = ['provider_name', 'address_line1', 'address_line2', 'address_line3', 'provider_phone']
    provider_details = list()
    for _key in list_data:
        #if provider_contacts_data.get(_key):
        provider_details.append(provider_contacts_data.get(_key))

    dated = datetime.now().strftime('%m/%d/%y')
    provider_website = provider_contacts_data.get('provider_website', '')
    provider_email = provider_contacts_data.get('provider_email', '')
    print("dated", dated)
    return dict(dated=dated, provider_details=provider_details, provider_website=provider_website,
                business_title=business_title, logo=encoded_string, provider_email=provider_email)


def email_data():
    try:
        with open(EMAIL_FILE, 'r') as f:
            email_data_details = json.load(f)

        return dict(email_data_details)
    except:
        return dict()


def __create_pdf_file(data, filename='out'):
    config = pdfkit.configuration(wkhtmltopdf=PATH_WKTHMLTOPDF)
    file_paths = []

    # ----- Main pdf -------
    rendered = loader.render_to_string('pdf_base.html', data)

    pdf_file_path = os.path.join(TEMP_DIR, secure_filename(filename))
    pdfkit.from_string(rendered, pdf_file_path, configuration=config, css=CSS)
    file_paths.append(pdf_file_path)

    # ---- Other pdfs
    pages = ["page_two_content",
             "page_three_content",
             "page_four_content"]

    for page in range(len(pages)):
        if data.get(pages[page]):
            page_number = page + 2
            _data = data.copy()
            _data.update({"page_data": data.get(pages[page]), "page_number": page_number})
            rendered = loader.render_to_string('pdf_blank_base.html', _data)
            pdf_filename = pages[page] + '.pdf'
            filename_path = os.path.join(TEMP_DIR, pdf_filename)
            # with open(pdf_filename, 'w') as f:
            #     f.write(rendered)
            # with open(os.path.join(TEMP_DIR, pages[page] + 'sample.html'), 'w') as f:
            #     f.write(rendered)
            pdfkit.from_string(rendered, filename_path, configuration=config, css=CSS)
            file_paths.append(filename_path)

    print(file_paths)

    filename_path_out = os.path.join(PDF_FILES_DIR, secure_filename(filename) + ".pdf")
    merger = PdfFileMerger()
    for pdf in file_paths:
        merger.append(pdf)
    merger.write(filename_path_out)
    merger.close()
    return filename_path_out


def create_pdf_util(data, filename='out'):
    with open(os.path.join(TEMP_DIR, secure_filename(filename) + '.json'), 'w') as f:
        json.dump(data, f)

    filename_path_out = __create_pdf_file(data, filename=filename)
    pdf_filename = secure_filename(filename) + ".pdf"

    with open(filename_path_out, 'rb') as f:
        response = HttpResponse(f.read(), content_type='application/pdf')
    response['Content-Type'] = 'application/pdf'
    response['Content-Disposition'] = 'inline;filename={}'.format(pdf_filename)
    return response


def create_pdf_and_send_email(data, filename):
    filename_path_out = __create_pdf_file(data, filename=filename)
    pdf_filename = secure_filename(filename) + ".pdf"

    email_data_to_get = email_data()
    email_host = email_data_to_get.get('email_host')
    email_port = email_data_to_get.get('email_port')
    email_subject = email_data_to_get.get('email_subject')
    email_body = email_data_to_get.get('email_body', '')
    email_host_user = email_data_to_get.get('email_host_user')
    email_host_password = email_data_to_get.get('email_host_password')
    client_email = data.get('client_email')

    html = """\
    <html>
      <head></head>
      <body>
        <p>
           {}
        </p>
      </body>
    </html>
    """
    # Send email
    try:
        email_client = SendEmail(email_host, email_port, email_host_user, email_host_password)
        subject = email_subject
        body = html.format(email_body.replace('\n', '<br>'))
        sender = email_host_user
        recipient = client_email
        ret, msg = email_client.send_email(sender, recipient, subject, body, filename_path_out, pdf_filename)
        os.remove(filename_path_out)
        return ret, msg
    except Exception as e:
        return None, str(e)


def save_inmem_file(file_object, file_name):
    with open(file_name, 'wb') as f:
        f.write(file_object.read())