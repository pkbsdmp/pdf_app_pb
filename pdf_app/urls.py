"""pdf_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home import views as home_view
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view.index, name='home'),
    path('email-settings', home_view.email_settings, name='email_settings'),
    path('provider-contact', home_view.provider_contact, name='provider_contact'),
    path('load-pdf', home_view.load_pdf, name='load_pdf'),
    path('create_pdf', home_view.create_pdf_data, name='create_pdf'),
    path('select-pdf-file', home_view.select_pdf_file, name='select_pdf_file'),
    path('delete-pdf-file', home_view.delete_pdf_file, name='delete_pdf_file'),
    path('accounts/', include('django.contrib.auth.urls')), # new
]
