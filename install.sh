apt update -y
apt install net-tools -y
apt install python3-virtualenv -y
apt install wkhtmltopdf -y
apt install git  -y
mkdir /var/www/pdf_app_dir
cd /var/www/pdf_app_dir
pwd
git clone https://pkbsdmp@bitbucket.org/pkbsdmp/pdf_app_pb.git
virtualenv venv
venv/bin/pip3 install -r pdf_app_pb/requirements.txt
nohup venv/bin/python pdf_app_pb/manage.py runserver 0.0.0.0:8000 2> /dev/null &
echo "******************************"
echo "*********** SUCCESS **********"
echo "******************************"
echo ""
echo "Check your app at: http://$(curl -s ifconfig.me):8000"
echo ""