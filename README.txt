Get a Debian / Ubuntu system
1. Install wkhtmltopdf:
    Run command:
    $ apt-get install wkhtmltopdf

2. Install python modules
    Python might be installed already in the system
    Now just install python modules required by running following command

    $ pip3 install -r requirements.txt

3. Run server in the backend
    $ nohup python manage.py runserver 0.0.0.0:8000 2> /dev/null &